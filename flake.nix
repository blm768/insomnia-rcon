{
  description = "A tool to keep a server awake based on data retrieved via RCON";
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.follows = "nixCargoIntegration/nixpkgs";
    nixCargoIntegration.url = "github:yusdacra/nix-cargo-integration";
  };
  outputs = inputs:
    let
      lib = inputs.nixpkgs.lib;
      cargoOut = inputs.nixCargoIntegration.lib.makeOutputs {
        root = ./.;
      };
      moduleOut = {
        nixosModules.insomnia-rcon = { config, lib, pkgs, ... }:
          with lib;
          let
            cfg = config.services.insomnia-rcon;
          in
            {
              options.services.insomnia-rcon = {
                enable = mkOption {
                  type = types.bool;
                  default = false;
                  description = "Enables the service";
                };
                address = mkOption {
                  type = types.str;
                  example = "192.168.0.3:25565";
                  description = "IP address and port of the RCON server";
                };
                password = mkOption {
                  # TODO: shouldn't put this in the Nix store!
                  type = types.str;
                  description = "RCON password";
                };
                status = {
                  command = mkOption {
                    type = types.str;
                    description = "Command to retrieve status";
                  };
                  regex = mkOption {
                    type = types.str;
                    description = "Regex to match results of status command (server remains awake if matched)";
                  };
                };
              };
              config = mkIf cfg.enable {
                environment.etc."insomnia-rcon/config.toml".text = ''
                  addr = ${builtins.toJSON cfg.address}
                  password = ${builtins.toJSON cfg.password}
                  status_command = ${builtins.toJSON cfg.status.command}
                  status_regex = ${builtins.toJSON cfg.status.regex}
                '';
                systemd.services.insomnia-rcon = {
                  after = [ "network-online.target" ];
                  wants = [ "network-online.target" ];
                  serviceConfig = {
                    Type = "simple";
                    ExecStart = "${cargoOut.packages.x86_64-linux.insomnia-rcon}/bin/insomnia-rcon -c /etc/insomnia-rcon/config.toml";
                  };
                  wantedBy = [ "multi-user.target" ];
                  enable = true;
                };
              };
            };
      };
    in
      lib.recursiveUpdate cargoOut moduleOut;
}
