use std::time::Duration;

use regex::Regex;

use serde::{Deserialize, Deserializer, Serialize, Serializer};
#[derive(Debug, Deserialize, Serialize)]
pub struct Config {
    // TODO: should have stronger typing.
    pub addr: String,
    pub password: String,
    #[serde(default = "default_poll_interval")]
    pub poll_interval: Duration,
    pub connect_timeout: Option<Duration>,

    pub status_command: String,
    #[serde(
        deserialize_with = "regex_from_string",
        serialize_with = "regex_to_string"
    )]
    pub status_regex: Regex,
}

fn default_poll_interval() -> Duration {
    Duration::from_secs(10)
}

fn regex_from_string<'de, D: Deserializer<'de>>(deserializer: D) -> Result<Regex, D::Error> {
    struct RegexDeserializer;

    impl<'de> serde::de::Visitor<'de> for RegexDeserializer {
        type Value = Regex;
        fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
            formatter.write_str("a string containing a regular expression")
        }
        fn visit_str<E: serde::de::Error>(self, s: &str) -> Result<Self::Value, E> {
            s.parse()
                .map_err(|e| serde::de::Error::custom(format!("invalid regular expression: {}", e)))
        }
    }

    deserializer.deserialize_str(RegexDeserializer)
}

fn regex_to_string<S: Serializer>(regex: &Regex, serializer: S) -> Result<S::Ok, S::Error> {
    serializer.serialize_str(regex.as_str())
}
