use std::fmt::{Display, Formatter};
use std::path::Path;
use std::sync::{Arc, Mutex};
use std::time::Duration;

use clap::{App, Arg};

use insomnia::{self, EnumSet, InhibitionManager, LockType};

use log::{error, info};

use insomnia_rcon::config::Config;
use insomnia_rcon::rcon::{RconMonitor, Status};

fn get_config() -> Result<Config, LoadConfigError> {
    let app = App::new("insomnia-rcon")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Ben Merritt <blm768@gmail.com>")
        .about("Inhibits system sleep based on the results of RCON commands")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("location of the configuration file")
                .takes_value(true),
        );
    let matches = app.get_matches();

    let config_path = Path::new(matches.value_of("config").unwrap_or("config.toml"));
    let text = std::fs::read_to_string(config_path).map_err(LoadConfigError::LoadFailed)?;
    toml::from_str(&text).map_err(LoadConfigError::ParseFailed)
}

async fn main_loop<IManager>(
    inhibition_manager: IManager,
    mut rcon: RconMonitor,
    config: Config,
) -> Result<(), RunError>
where
    IManager: InhibitionManager + Send + 'static,
{
    let inhibition_manager = Arc::new(Mutex::new(inhibition_manager));
    let get_lock = move || {
        inhibition_manager
            .lock()
            .expect("failed to lock InhibitionManager mutex")
            .lock(
                EnumSet::only(LockType::AutomaticSuspend),
                "insomnia-rcon",
                "server is active",
            )
            .map_err(|e| error!("Unable to inhibit sleep: {}", e))
            .ok()
    };

    let mut lock: Option<IManager::Lock> = None;
    let mut last_status = Status::Inactive;
    loop {
        let status = rcon.status().await;
        match status {
            Ok(status) => {
                if status != last_status {
                    log::info!(
                        "Status changed: {}",
                        match status {
                            Status::Active => "active",
                            Status::Inactive => "inactive",
                        }
                    );
                }
                last_status = status;

                match status {
                    Status::Inactive => {
                        lock = None;
                    }
                    Status::Active => {
                        if lock.is_none() {
                            lock = tokio::task::spawn_blocking(get_lock.clone())
                                .await
                                .map_err(|e| RunError::Other(Box::new(e)))?;
                        }
                    }
                }
            }
            Err(e) => {
                // TODO: clear lock (if present) after a timeout.
                error!("Unable to get server status: {}\n", e);
            }
        }
        tokio::time::sleep(config.poll_interval).await;
    }
    // TODO: handle graceful shutdown
    //rcon.close().await;
}

fn is_retryable_connect_failure(err: &rercon::Error) -> bool {
    match err {
        rercon::Error::IO(e) => {
            use std::io::ErrorKind;
            match e.kind() {
                ErrorKind::ConnectionRefused => true,
                ErrorKind::ConnectionReset => true,
                ErrorKind::ConnectionAborted => true,
                ErrorKind::TimedOut => true,
                _ => false,
            }
        }
        _ => false,
    }
}

async fn connect_rcon(config: &Config) -> Result<RconMonitor, rercon::Error> {
    const RECONNECT_INTERVAL: Duration = Duration::from_secs(30);
    loop {
        match RconMonitor::new(config).await {
            Ok(monitor) => {
                log::info!("Connected to RCON host");
                return Ok(monitor);
            }
            Err(e) => {
                if is_retryable_connect_failure(&e) {
                    log::error!("{}", e);
                    tokio::time::sleep(RECONNECT_INTERVAL).await;
                    continue;
                } else {
                    return Err(e);
                }
            }
        }
    }
}

fn run() -> Result<(), RunError> {
    let config = get_config().map_err(RunError::LoadConfig)?;

    info!("Connecting to {}", config.addr);

    // TODO: inhibition manager should support an async API.
    let inhibition_manager = insomnia::manager().map_err(RunError::InitManager)?;
    let runtime = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .map_err(RunError::InitTokio)?;

    runtime.block_on(async move {
        let rcon = connect_rcon(&config).await.map_err(RunError::InitRcon)?;
        main_loop(inhibition_manager, rcon, config).await
    })
}

type ManagerError = <insomnia::platform::InhibitionManager as InhibitionManager>::Error;

#[derive(Debug)]
enum RunError {
    LoadConfig(LoadConfigError),
    InitManager(ManagerError),
    InitRcon(rercon::Error),
    InitTokio(tokio::io::Error),
    Other(Box<dyn std::error::Error>),
}

impl Display for RunError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::LoadConfig(err) => err.fmt(f),
            Self::InitManager(err) => write!(
                f,
                "Unable to initialize power management inhibition manager: {}",
                err
            ),
            Self::InitRcon(err) => write!(f, "Unable to initialize RCON: {}", err),
            Self::InitTokio(err) => write!(f, "Unable to initialize async runtime: {}", err),
            Self::Other(err) => write!(f, "Unexpected error: {:?}", err),
        }
    }
}

impl std::error::Error for RunError {}

#[derive(Debug)]
enum LoadConfigError {
    LoadFailed(std::io::Error),
    ParseFailed(toml::de::Error),
}

impl Display for LoadConfigError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            Self::LoadFailed(err) => write!(f, "Unable to load config file: {}", err),
            Self::ParseFailed(err) => write!(f, "Unable to parse config file: {}", err),
        }
    }
}

impl std::error::Error for LoadConfigError {}

fn main() {
    env_logger::init();

    if let Err(err) = run() {
        error!("{}", err);
        std::process::exit(1);
    }
}
