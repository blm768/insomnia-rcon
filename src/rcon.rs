use regex::Regex;

use rercon::{ReConnection, Settings};

use crate::config::Config;

pub struct RconMonitor {
    connection: ReConnection,
    status_command: String,
    status_regex: Regex,
}

impl RconMonitor {
    pub async fn new(config: &Config) -> Result<Self, rercon::Error> {
        let mut settings = Settings::default();
        if let Some(timeout) = config.connect_timeout {
            settings.connect_timeout = timeout;
        }
        let connection = ReConnection::open(&config.addr, &config.password, settings).await?;
        Ok(Self {
            connection,
            status_command: config.status_command.clone(),
            status_regex: config.status_regex.clone(),
        })
    }

    pub async fn status(&mut self) -> Result<Status, rercon::Error> {
        let result = self.connection.exec(&self.status_command).await?;
        Ok(match self.status_regex.is_match(&result) {
            true => Status::Active,
            false => Status::Inactive,
        })
    }

    pub async fn close(self) {
        self.connection.close().await;
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Status {
    Active,
    Inactive,
}
